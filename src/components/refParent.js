import React, { Component } from "react";
import RefChild from "./refChild";

class RefParent extends Component {
    constructor() {
        super();
        this.childRef = React.createRef();
    }
    btnClicked = () => {
        const child = this.childRef.current._getInputRef();
        child.value ='aaaa';
        child.focus();
        // console.log(child);
    }


    render() {
        return <div>
            <RefChild ref={this.childRef}></RefChild>
            <button onClick={this.btnClicked}>Click Me</button>
        </div>
    }
}

export default RefParent;