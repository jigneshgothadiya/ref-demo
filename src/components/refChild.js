import React, { Component } from "react";

class RefChild extends Component{
    constructor(){
        super();
        this.inputRef = React.createRef();
    }

    _getInputRef(){
        return this.inputRef.current;
    }
    render(){
        return <div>
            <input type="text" ref={this.inputRef}/>
        </div>
    }
}

export default RefChild;