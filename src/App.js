import './App.css';
import RefParent from './components/refParent';
function App() {
  return (
    <div className="App">
      <RefParent/>
    </div>
  );
}

export default App;
